package stjepan.weather_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CityTemperatureActivity extends Activity implements Callback<SearchResult> {

    public static final String KEY_CITY = "city";

    @BindView(R.id.tvCityName)
    TextView tvCityName;
    @BindView(R.id.lvWeather)
    ListView lvWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_temperature);
        ButterKnife.bind(this);
        this.handleExtraData(this.getIntent());
    }

    private void handleExtraData(Intent startingIntent){
        if (startingIntent != null){
            if (startingIntent.hasExtra(KEY_CITY)){
                this.showTemperature(getIntent().getStringExtra(KEY_CITY));
            }
        }
    }

    private void showTemperature(String cityName) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OWMapi.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OWMapi api = retrofit.create(OWMapi.class);
        Call<SearchResult> request;
        request = api.getWeatherInfo(cityName, OWMapi.UNIT, OWMapi.API_KEY);
        request.enqueue(this);
    }

    @Override
    public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
        SearchResult result = response.body();
        WeatherAdapter adapter = new WeatherAdapter(result);
        this.lvWeather.setAdapter(adapter);
        tvCityName.setText(result.getCity().getmName());
    }

    @Override
    public void onFailure(Call<SearchResult> call, Throwable t) {
        Log.e("Fail", t.getMessage());
    }

    @OnItemClick(R.id.lvWeather)
    public void showDetails(int position)
    {
        WeatherInfoList weatherInfoList = (WeatherInfoList) this.lvWeather.getAdapter().getItem(position);
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_DATE, weatherInfoList.getDt_txt());
        intent.putExtra(DetailsActivity.KEY_TEMPERATURE, String.valueOf(weatherInfoList.getMain().getmTemp()));
        intent.putExtra(DetailsActivity.KEY_HUMIDITY, String.valueOf(weatherInfoList.getMain().getmHumidity()));
        intent.putExtra(DetailsActivity.KEY_PRESSURE, String.valueOf(weatherInfoList.getMain().getmPressure()));
        intent.putExtra(DetailsActivity.KEY_WINDSPEED, String.valueOf(weatherInfoList.getWind().getmSpeed()));
        this.startActivity(intent);
    }
}

