package stjepan.weather_app;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class Clouds {
    @SerializedName("all") private int mAll;

    public Clouds(){

    }
    public Clouds(int mAll) {
        this.mAll = mAll;
    }

    public int getmAll() {
        return mAll;
    }
}
