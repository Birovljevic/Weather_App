package stjepan.weather_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.etCityName) EditText etCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnSearch)
    public void searchCityWeather(){
        String cityName = this.etCityName.getText().toString();

        if (cityName.equals("")){
            Toast.makeText(this, "Bad input", Toast.LENGTH_SHORT).show();
        }
        else{
            Intent intent = new Intent(this, CityTemperatureActivity.class);
            intent.putExtra(CityTemperatureActivity.KEY_CITY, cityName);
            this.startActivity(intent);
        }
    }
}

// public static final OWMKey = 5d484613a764056a49d2f5d6a8c401e4
/*
public interface OWMapi {

    String BASE_URL = "api.openweathermap.org/data/2.5/";

    @GET("weather")
    Call<WeatherInfo> getWeatherInfo(@Query("q") String cityName, @Query("appid") String appKey);
}
 */