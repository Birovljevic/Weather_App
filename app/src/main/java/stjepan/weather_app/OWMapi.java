package stjepan.weather_app;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Stjepan on 6.11.2017..
 */

public interface OWMapi {

    //String FULL_URL = "api.openweathermap.org/data/2.5/";
    String BASE_URL = "http://api.openweathermap.org/";
    String API_KEY = "5d484613a764056a49d2f5d6a8c401e4";
    String UNIT = "metric";
    String IMAGE_URL = "http://openweathermap.org/img/w/";


    @GET("data/2.5/forecast")
    Call<SearchResult> getWeatherInfo(
            @Query("q") String cityName,
            @Query("units") String UNIT,
            @Query("APPID") String API_KEY);
}
