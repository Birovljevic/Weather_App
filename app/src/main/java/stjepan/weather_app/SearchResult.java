package stjepan.weather_app;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class SearchResult {
    @SerializedName("cod") private int mCod;
    @SerializedName("message") private double mMessage;
    @SerializedName("cnt") private int mCnt;
    @SerializedName("list") private List<WeatherInfoList> mWeatherInfoList;
    @SerializedName("city") private City mCity;

    public SearchResult(){

    }

    public SearchResult(int cod, double message, int cnt, List<WeatherInfoList> weatherInfoListList, City city){
        this.mCod = cod;
        this.mMessage = message;
        this.mCnt = cnt;
        this.mWeatherInfoList = weatherInfoListList;
        this.mCity = city;
    }

    public int getCod() {
        return this.mCod;
    }

    public double getMessage() {
        return this.mMessage;
    }

    public int getCnt() {
        return this.mCnt;
    }

    public List<WeatherInfoList> getWeatherInfoList() {
        return this.mWeatherInfoList;
    }

    public City getCity() {
        return this.mCity;
    }
}




