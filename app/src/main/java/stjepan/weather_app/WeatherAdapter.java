package stjepan.weather_app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class WeatherAdapter extends BaseAdapter{

    SearchResult mWeather;

    public WeatherAdapter(SearchResult mWeather) {
        this.mWeather = mWeather;
    }

    @Override
    public int getCount() {
        return this.mWeather.getWeatherInfoList().size();
    }

    @Override
    public Object getItem(int position) {
        return this.mWeather.getWeatherInfoList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        WeatherHolder holder;
        if (convertView == null){
            convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_forecast, parent, false);
            holder = new WeatherHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (WeatherHolder) convertView.getTag();
        }

        WeatherInfoList result = this.mWeather.getWeatherInfoList().get(position);

        holder.tvDate.setText(result.getDt_txt());
        holder.tvTemperature.setText("Current Temp: " + String.valueOf(result.getMain().getmTemp()));
        holder.tvTemperatureMin.setText("Min Temp: " + String.valueOf(result.getMain().getmTempMax()));
        holder.tvTemperatureMax.setText("Max Temp: " + String.valueOf(result.getMain().getmTempMin()));

        for (Weather Weather : result.getWeather()) {

            /*
            holder.tvWeatherMain.setText(Weather.getmMain() + ":");
            holder.tvDescription.setText(Weather.getmDescription());
             */


            holder.tvMainAndDescription.setText(Weather.getmMain() + ": " + Weather.getmDescription());


            Picasso.with(parent.getContext())
                    .load(OWMapi.IMAGE_URL + Weather.getmIcon() + ".png")
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_not_found)
                    .into(holder.ivWeatherIcon);
        }

        return convertView;
}
    static class WeatherHolder
    {
        @BindView(R.id.tvDate) TextView tvDate;
        //@BindView(R.id.tvWeatherMain) TextView tvWeatherMain;
        @BindView(R.id.tvTemperature) TextView tvTemperature;
        @BindView(R.id.tvTemperatureMin) TextView tvTemperatureMin;
        @BindView(R.id.tvTemperatureMax) TextView tvTemperatureMax;
        //@BindView(R.id.tvDescription) TextView tvDescription;
        @BindView(R.id.ivWeatherIcon) ImageView ivWeatherIcon;
        // main + description
        @BindView(R.id.tvMainAndDescription) TextView tvMainAndDescription;

        public WeatherHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }
}
